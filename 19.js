const members = [10, 5, 3, 2];
const backlog = [300, 250, 150, 100];
const dateEnter = prompt("Enter the deadline date as dd.mm.yyyy");
// Supporting function for calculation of the number of working days-----
const numberBetweenDates = (eDate) => {
    const start = new Date();
    const end = (new Date(eDate)).setDate((new Date(eDate)).getDate() + 1);
    let dayCount = 0;
    while (end >= start) {
        if (start.getDay() !== 0 && start.getDay() !== 6) { dayCount++ };
        start.setDate(start.getDate() + 1);
    }
    return dayCount;
};
// General function------------------------------------------------------
const esimateBacklogDone = (membersArr, backlogArr, dateInput) => {
    const hoursEstim = 8 * backlogArr.reduce((sum, item) => sum + item, 0) / membersArr.reduce((sum, item) => sum + item, 0);
    const endDate = `${dateInput.slice(3, 5)}.${dateInput.slice(0, 2)}.${dateInput.slice(6, 10)}`;
    const hoursToDeadline = 8 * numberBetweenDates(endDate);
    if (hoursEstim <= hoursToDeadline) {
        alert(`Дедлайн: ${dateInput}. Все задачи будут успешно выполнены за ${(hoursToDeadline - hoursEstim) / 8} рабочих дней до наступления дедлайна!`)
    } else {
        alert(`Дедлайн: ${dateInput}. Команде разработчиков придется потратить дополнительно ${hoursEstim - hoursToDeadline} часов после дедлайна, чтобы выполнить все задачи в беклоге`)
    };
};

esimateBacklogDone(members, backlog, dateEnter);


